const sortInDescendingOrder = (arr) => {
    return arr.sort((a, b) => b.code - a.code);
}

const list = [
    {
        name: "test 10",
        country: "usa",
        code: 100
    },
    {
        name: "test 10",
        country: "usa",
        code: 1
    },
    {
        name: "test 10",
        country: "usa",
        code: 44
    },
    {
        name: "test 10",
        country: "usa",
        code: 200
    }
];

console.log(sortInDescendingOrder(list));